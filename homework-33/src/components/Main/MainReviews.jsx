import React from "react";
import './mainReviews.css';
import './mainReviewsName.css';
import './mainReviewsName1.css';
import './mainReviewsNameName.css';
import './mainReviewNameCount.css';
import './mainReviewsReviews.css';
import './mainReviewsReviews1.css';
import './mainReviewsReviews1Img.css';
import './mainReviewsReviews1Info.css';
import './mainReviewsReviews1Header.css';
import './mainReviewsReviews1HeaderNameClient.css';
import './mainReviewsReviews1HeaderNameRating.css';
import './mainReviewsReviews1Content.css';
import './mainReviewsClient2.css';
import './mainReviewsReviews1ContentText.css';
import './mainReviewsReviews1ContentText1.css';
import './mainSeparator.css';



function MainReviews(){

 return(
    <div className="main__reviews">
    <div className="main__reviews-name">
        <div className="main__review-name-1">
            <h3 className="main__review-name-name"> Отзывы </h3>
            <span className="main__review-name-count">425</span>
        </div>
    </div>
    <div className="main__reviews-reviews">
        <div className="main__reviews-reviews-1">
            <img className="main__reviews-reviews-1-img" alt="Клиент1" src="review-photo/review-1.jpeg"/>
            <div className="main__reviews-reviews-1-info">
                <div className="main__reviews-reviews-1-header">
                    <span className="main__reviews-reviews-1-header-name-client"> Марк Г. </span>
                    <div className="main__reviews-reviews-1-header-name-rating">
                        <img className="main__reviews-client-2" alt="Оценка" src="review-photo/star-5.png"/>
                    </div>
                </div>
                <div className="main__reviews-reviews-1-content">
                    <p className="main__reviews-reviews-1-content-text"><span
                            className="main__reviews-reviews-1-content-text1"> Опыт использования: </span>менее
                        месяца </p>
                    <p className="main__reviews-reviews-1-content-text "><span
                            className="main__reviews-reviews-1-content-text1 "> Достоинства:</span><br/> OLED экран,
                        Дизайн, Система камер, Шустрый А15, Заряд держит долго </p>
                    <p className="main__reviews-reviews-1-content-text"><span
                            className="main__reviews-reviews-1-content-text1 ">Недостатки: </span> <br/>
                        К самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь а
                        если нужно переносить фото с компа, то это только через itunes, который урезает качество
                        фотографий исходное </p>
                </div>
            </div>
        </div>
        <hr className="main__separator"/>

        <div className="main__reviews-reviews-1">
            <img className="main__reviews-reviews-1-img" alt="Клиент1" src="review-photo/review-2.jpeg"/>
            <div className="main__reviews-reviews-1-info ">
                <div className="main__reviews-reviews-1-header">
                    <span className="main__reviews-reviews-1-header-name-client"> Марк Г. </span>
                    <div className="main__reviews-reviews-1-header-name-rating">
                        <img className="main__reviews-client-2 " alt="Оценка" src="review-photo/star-4.png"/>
                    </div>
                </div>
                <div className="main__reviews-reviews-1-content ">
                    <p className="main__reviews-reviews-1-content-text"><span
                            className="main__reviews-reviews-1-content-text1"> Опыт использования: </span>менее
                        месяца </p>
                    <p className="main__reviews-reviews-1-content-text"><span
                            className="main__reviews-reviews-1-content-text1"> Достоинства:</span><br/> OLED экран,
                        Дизайн, Система камер, Шустрый А15, Заряд держит долго </p>
                    <p className="main__reviews-reviews-1-content-text"><span
                            className="main__reviews-reviews-1-content-text1">Недостатки: </span> <br/>Плохая
                        ремонтопригодность </p>

                </div>

            </div>

        </div>

    </div>
   </div>
 )
 
}

export default MainReviews;