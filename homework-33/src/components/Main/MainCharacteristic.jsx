import React from "react";
import './mainCharacteristic.css';
import './mainCharacteristicLeft.css';
import './mainCharacteristicName.css';
import './mainCharacteristicImgColors.css';
import './mainCharacteristicColor.css';
import './mainCharacteristicColor1.css';
import './imgGuidance.css';
import './imgSelect.css';
import './mainCharacteristicMemory.css';
import './mainBtnMemory.css';
import './mainBtnMemory1.css';
import './btnGuidance.css';
import './btnGuidance1.css';
import './btnSelect.css';
import './mainCharacteristicProduct.css';
import './mainCharacteristicProductName.css';
import './mainCharacteristicProductName1.css';
import './mainProduct.css';
import './mainDescriptionText.css';
import './mainDescriptionTextName1.css';
import './mainDescriptionParagraph.css';
import './mainSideBar.css';
import './mainSideBarImportant.css';
import './mainSideBarImportantPrice.css';
import './mainSideBarImportantPrice1.css';
import './mainSideBarImportantPriceOld.css';
import './mainSideBarImportantFavorite.css';
import './mainSideBarImportantFavoriteIcon.css';
import './mainSideBarImportantActualPrice.css';
import './mainSideBarDelivery.css';
import './mainSideBarOptionDelivery.css';
import './mainSideBarBtnImportant.css';
import './mainSideBarAdvertisement.css';
import './mainSideBarAdvertisementName.css';
import './mainSideBarAdvertisementList.css';
import './mainSideBarAdvertisementListFrame1.css';
import './mainOther.css';

function MainCharacteristic() {

    return (
        <div className="main__characteric">
            <div className="main__characteristic-left">
                <h3 className="main__characteristic-name">Цвет товара: синий</h3>
                <div className="main__characteric-img-colors">
                    <input id="radio-1" type="radio" name="color" />
                    <div className="main__characteric-color img-guidance">
                        <label htmlFor="radio-1"><img className="main__characteric-color-1 " alt="цвет1"
                            src="main-photo-product-color/color-1.webp" /> </label>
                    </div>
                    <input id="radio-2" type="radio" name="color" />
                    <div className="main__characteric-color img-guidance img-select">
                        <label htmlFor="radio-2"><img className="main__characteric-color-1" alt="цвет2"
                            src="main-photo-product-color/color-2.webp" /> </label>
                    </div>
                    <input id="radio-3" type="radio" name="color" />
                    <div className="main__characteric-color img-guidance ">
                        <label htmlFor="radio-3"> <img className="main__characteric-color-1 " alt="цвет3"
                            src="main-photo-product-color/color-3.webp" /> </label>
                    </div>
                    <input id="radio-4" type="radio" name="color" />
                    <div className="main__characteric-color  img-guidance">
                        <label htmlFor="radio-4"><img className=" main__characteric-color-1 " alt="цвет4"
                            src="main-photo-product-color/color-4.webp" /> </label>
                    </div>
                    <input id="radio-5" type="radio" name="color" />
                    <div className="main__characteric-color  img-guidance">
                        <label htmlFor="radio-5"><img className="main__characteric-color-1 " alt="цвет5"
                            src="main-photo-product-color/color-5.webp" /> </label>
                    </div>
                </div>
                <div className="main__characteristic-memory">
                    <h3 className="main__characteristic-name"> Конфигурация памяти: 128 ГБ </h3>
                    <div className="main__btn-memory">
                        <div> <button className="main__btn-memory-1 btn-guidance">128 ГБ</button> </div>
                        <div> <button className="main__btn-memory-1 btn-guidance btn-select">256 ГБ</button> </div>
                        <div> <button className="main__btn-memory-1 btn-guidance">512 ГБ</button> </div>
                    </div>
                </div>
                <div className="main__characteristic-product">
                    <div className="main__characteriscic-producrt-name">
                        <h3 className="main__characteriscic-producrt-name1">Характеристики товара </h3>
                    </div>
                    <div className="main__product">
                        <ul>
                            <li> Экран: <b>6.1</b> </li>
                            <li> Встроенная память: <b>128 ГБ </b> </li>
                            <li> Операционная система: <b>iOS 15</b> </li>
                            <li> Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi </b> </li>
                            <li> Процессор: <b><a target="blank" href="https://ru.wikipedia.org/wiki/Apple_A15"> Apple
                                A15
                                Bionic</a></b> </li>
                            <li>Вес: <b>173 г</b> </li>
                        </ul>
                    </div>
                </div>
                <div className="main__desctiption-text">
                    <div className="main__discription-text-name">
                        <h3 className="main__discription-text-name-1">Описание</h3>
                    </div>
                    <div className="main__description-paragraph">
                        <div>
                            Наша самая совершенная система двух камер.<br /> Особый взгляд на прочность
                            дисплея. <br /> Чип, с которым всё супербыстро.<br /> Аккумулятор держится заметно дольше.
                            <br /><i>iPhone
                                13
                                - сильный мира всего. </i>
                        </div>
                        <div >
                            Мы разработали совершенно новую схему расположения и развернулиобъективы на 45 градусов.
                            Благодаря этому
                            внутри
                            корпуса
                            поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной камеры.
                            Кроме
                            того, мы
                            освободили
                            место для системы оптической стабилизацииизображения сдвигом матрицы. И повысили скорость
                            работы
                            матрицы
                            насверхширокоугольной камере.
                        </div>
                        <div >
                            Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая
                            широкоугольная камера улавливает на 47% больше света для
                            более качественных фотографий и видео. Новая оптическая стабилизация сосдвигом матрицы
                            обеспечит
                            чёткие
                            кадры
                            даже в неустойчивом положении.
                        </div>
                        <div >
                            Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и
                            изменения
                            резкости. Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте
                            съёмки,
                            создавая
                            красивый
                            эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на
                            другогочеловека или
                            объект, который появился в кадре. Теперь ваши видео будут смотретьсякак настоящее кино.
                        </div>
                    </div>
                </div>
            </div>
            <div className="main__sidebar">
                <div className="main__sidebar-important">
                    <div className="main__sidebar-important-price">
                        <div className="main__sidebar-important-price-1">
                            <span className="main__sidebar-important-price-old "> 75 990</span>
                            <span className="main__sidebar-important-favorite"><svg className="main__sidebar-important-favorite-icon" width="26" height="20"
                                viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M2.78502 2.57269C5.17872 0.274736 9.04661 0.274736 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.274736 22.8216 0.274736 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63043 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63043 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" />
                            </svg></span>
                        </div>
                        <span className="main__sidebar-important-actual-price "> 67 990₽ </span>
                    </div>
                    <div className="main__sidebar-delivery">
                        <span className="main__sidebar-option-delivery "> Самовывоз в четверг, 1 сентября — <b>бесплатно</b></span>
                        <span className="main__sidebar-option-delivery "> Курьером в четверг, 1 сентября — <b> бесплатно</b></span>
                    </div>
                    <button id="add-cart" className="main__sidebar-btn-important btn-guidance1"> Добавить в корзину </button>
                </div>
                <div className="main__sidebar-advertisement">
                    <span className="main__sidebar-advertisement-name"> Реклама</span>
                    <div className="main__sidebar-advertisement-list">
                        <iframe scrolling="no" className="main__sidebar-advertisement-list-frame1" src="frame/ads.html"></iframe>
                        <iframe scrolling="no" className="main__sidebar-advertisement-list-frame1" src="frame/ads.html"></iframe>
                    </div>
                </div>
            </div>
        </div>

    )

}

export default MainCharacteristic;