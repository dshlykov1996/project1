import React from "react";
import './footerContent.css';
import './footer.css';
import './footerContentLine.css';
import './footerContentColor.css';
import './footerReturn.css';


function Footer(){

    return(
        <footer className="footer">
            <div className="footer__content">
            <span className="footer__content-line"> © ООО «<span className="footer__content_color">Цифровой</span> мир»,
                2018-2022.<br/> </span> Для уточненияинформации звоните по номеру <a href="tel:7 900 000 0000">+7 900 000 0000</a>, <br/>
            а предложения по сотрудничеству отправляйте на почту <a
                href="mailto:partner@mdigital-mir.com">partner@digital-mir.com</a> <br/>
            </div>
            <div className="footer__return">
            <a href="#market">Наверх</a>
        </div>
        </footer>
    )
}

export default Footer;