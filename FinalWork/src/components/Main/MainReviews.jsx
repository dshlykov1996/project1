import React from "react";
import './mainReviews.css';
import './mainReviewsName.css';
import './mainReviewsName1.css';
import './mainReviewsNameName.css';
import './mainReviewNameCount.css';
import './mainReviewsReviews.css';
import './mainReviewsReviews1.css';
import './mainReviewsReviews1Img.css';
import './mainReviewsReviews1Info.css';
import './mainReviewsReviews1Header.css';
import './mainReviewsReviews1HeaderNameClient.css';
import './mainReviewsReviews1HeaderNameRating.css';
import './mainReviewsReviews1Content.css';
import './mainReviewsClient2.css';
import './mainReviewsReviews1ContentText.css';
import './mainReviewsReviews1ContentText1.css';
import './mainSeparator.css';



function MainReviews() {
    const Reviews = [
        {
            id: 1,
            name: 'Марк Г.',
            photoClient: 'review-photo/review-1.jpeg',
            item1: 'Опыт использования: ',
            textItem1: 'менее месяца',
            item2: 'Достоинства:',
            textItem2: ' OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
            item3: 'Недостатки:',
            textItem3: 'К самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное ',
            ratingImg: 'review-photo/star-4.png'
        },
        {
            id: 2,
            name: 'Марк Г.',
            photoClient: 'review-photo/review-2.jpeg',
            item1: 'Опыт использования: ',
            textItem1: 'менее месяца',
            item2: 'Достоинства:',
            textItem2: ' OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
            item3: 'Недостатки:',
            textItem3: 'Плохая ремонтопригодность ',
            ratingImg: 'review-photo/star-5.png'
        }
    ]

    return (
        <div className="main__reviews">
            <div className="main__reviews-name">
                <div className="main__review-name-1">
                    <h3 className="main__review-name-name"> Отзывы </h3>
                    <span className="main__review-name-count">425</span>
                </div>
            </div>
            {Reviews.map((review, index) => {

                return (
                    <div className="main__reviews-reviews" key={review.id}>
                        <>
                            <div className="main__reviews-reviews-1">

                                <img className="main__reviews-reviews-1-img" alt="Клиент1" src={review.photoClient} />

                                <div className="main__reviews-reviews-1-info">

                                    <div className="main__reviews-reviews-1-header">
                                        <span className="main__reviews-reviews-1-header-name-client"> {review.name} </span>
                                        <div className="main__reviews-reviews-1-header-name-rating">
                                            <img className="main__reviews-client-2" alt="Оценка" src={review.ratingImg} />
                                        </div>
                                    </div>
                                    <div className="main__reviews-reviews-1-content">
                                        <p className="main__reviews-reviews-1-content-text"><span
                                            className="main__reviews-reviews-1-content-text1"> {review.item1} </span> {review.textItem1} </p>
                                        <p className="main__reviews-reviews-1-content-text "><span
                                            className="main__reviews-reviews-1-content-text1 "> {review.item2} </span><br /> {review.textItem2} </p>
                                        <p className="main__reviews-reviews-1-content-text"><span
                                            className="main__reviews-reviews-1-content-text1 "> {review.item3} </span> <br /> {review.textItem3} </p>

                                    </div>

                                </div>
                                


                            </div>
                            {index === Reviews.length - 1 ? ' ' : <hr className="main__separator" />}
                        </>
                    </div>
                )
            })}

        </div>
    )

}

export default MainReviews;