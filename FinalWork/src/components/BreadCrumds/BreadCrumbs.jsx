import React from 'react';
import './breadCrumbs.css';

function BreadCrumbs(props) {
        const item1 = props.item1;
        const item2 = props.item2;
        const item3 = props.item3;
        const item4 = props.item4;
        const next = props.next;
        return (
                <nav className="bread-crumbs">
                        <a href=""> {item1} </a>
                        <span> {next} </span>
                        <a href=""> {item2} </a>
                        <span>  {next} </span>
                        <a href=""> {item3} </a>
                        <span>  {next} </span>
                        <a href=""> {item4} </a>
                </nav>
        )
}

export default BreadCrumbs;