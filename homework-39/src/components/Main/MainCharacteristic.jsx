import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TO_CART, CLEAR_CART } from "../../redux/cartSlice";
import { ADD_TO_FAVORITE, CLEAR_FAVORITE } from "../../redux/favoriteSlice";
import './mainCharacteristic.css';
import './mainCharacteristicLeft.css';
import './mainCharacteristicName.css';
import './mainCharacteristicImgColors.css';
import './mainCharacteristicColor.css';
import './mainCharacteristicColorCheck.css';
import './imgGuidance.css';
import './imgSelect.css';
import './mainCharacteristicMemory.css';
import './mainBtnMemory.css';
import './mainBtnMemory1.css';
import './mainBtnMemoryCheck.css';
import './btnGuidance.css';
import './btnGuidance1.css';
import './btnSelect.css';
import './mainCharacteristicProduct.css';
import './mainCharacteristicProductName.css';
import './mainCharacteristicProductName1.css';
import './mainProduct.css';
import './mainDescriptionText.css';
import './mainDescriptionTextName1.css';
import './mainDescriptionParagraph.css';
import './mainSideBar.css';
import './mainSideBarImportant.css';
import './mainSideBarImportantPrice.css';
import './mainSideBarImportantPrice1.css';
import './mainSideBarImportantPriceOld.css';
import './mainSideBarImportantFavorite.css';
import './mainSideBarImportantFavoriteIcon.css';
import './mainSideBarImportantActualPrice.css';
import './mainSideBarDelivery.css';
import './mainSideBarOptionDelivery.css';
import './mainSideBarBtnImportant.css';
import './mainSideBarBtnImportantAdd.css';
import './mainSideBarAdvertisement.css';
import './mainSideBarAdvertisementName.css';
import './mainSideBarAdvertisementList.css';
import './mainSideBarAdvertisementListFrame1.css';
import './mainOther.css';
import './mainSideBarImportantPriceHeader.css';

const storage = window.localStorage;

function MainCharacteristic() {
    const dispatch = useDispatch();
    const cartCount = useSelector((state) => state.cart.cartItemsCount);
    const favoriteCount = useSelector((store) => store.favorite.favoriteCount);
    const addToCart = async () => {
        if (cartCount === 0) {
            dispatch(ADD_TO_CART());
        } else {
            dispatch(CLEAR_CART());
        }
    };
    const addToFavorite = () => {
        if (favoriteCount === 0) {
            dispatch(ADD_TO_FAVORITE());
        } else {
            dispatch(CLEAR_FAVORITE());
        }
    };
    useEffect(() => {
        storage.setItem('cartCount', cartCount);
    }, [cartCount]);

    useEffect(() => {
        storage.setItem('favoriteCount', favoriteCount);
    }, [favoriteCount]);

    const Btnmemory = [
        { id: 1, memorySize: "128 ГБ" },
        { id: 2, memorySize: "256 ГБ" },
        { id: 3, memorySize: "512 ГБ" }
    ];
    const Colors = [
        { id: 1, src: 'main-photo-product-color/color-1.webp', radioBtn: 'radio-1', color: 'Красный' },
        { id: 2, src: 'main-photo-product-color/color-2.webp', radioBtn: 'radio-2', color: 'Зеленый' },
        { id: 3, src: 'main-photo-product-color/color-3.webp', radioBtn: 'radio-3', color: 'Розоый' },
        { id: 4, src: 'main-photo-product-color/color-4.webp', radioBtn: 'radio-4', color: 'Синий' },
        { id: 5, src: 'main-photo-product-color/color-5.webp', radioBtn: 'radio-5', color: 'Белый' }
    ]


    const [currentColor, setCurrentSelect] = React.useState('0');
    const selectColor = (colorId) => {
        setCurrentSelect(colorId);
        console.log(colorId);

    }

    const [selectMemory, setSelectMemory] = React.useState('0');
    const changeMemory = (id) => {
        setSelectMemory(id);
        console.log(id);
    }

    return (
        <div className="main__characteric">
            <div className="main__characteristic-left">


                <h3 className="main__characteristic-name">Цвет товара: синий </h3>

                <div className="main__characteric-img-colors">

                    {Colors.map((color) => {
                        return (
                            <div key={color.id} onClick={() => selectColor(color.id)} className={color.id === currentColor ? 'main__characteric-color  ' : ' main__characteristic_color_check '} >
                                <img className="img-guidance" alt="цвет1" src={color.src} />
                            </div>
                        )


                    })}

                </div>
                <div className="main__characteristic-memory">

                    <h3 className="main__characteristic-name"> Конфигурация памяти: 128 ГБ </h3>
                    <div className="main__btn-memory">
                        {Btnmemory.map((btn) => {
                            return (

                                <div key={btn.id} selected={selectMemory === btn.id}   >
                                    <button onClick={() => changeMemory(btn.id)} className={btn.id === selectMemory ? 'main__btn_memory_check ' : 'main__btn-memory-1'}>
                                        {btn.memorySize} </button> </div>
                            )

                        })}
                    </div>
                </div>
                <div className="main__characteristic-product">
                    <div className="main__characteriscic-producrt-name">
                        <h3 className="main__characteriscic-producrt-name1">Характеристики товара </h3>
                    </div>
                    <div className="main__product">
                        <ul>
                            <li> Экран: <b>6.1</b> </li>
                            <li> Встроенная память: <b>128 ГБ </b> </li>
                            <li> Операционная система: <b>iOS 15</b> </li>
                            <li> Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi </b> </li>
                            <li> Процессор: <b><a target="blank" href="https://ru.wikipedia.org/wiki/Apple_A15"> Apple
                                A15
                                Bionic</a></b> </li>
                            <li>Вес: <b>173 г</b> </li>
                        </ul>
                    </div>
                </div>
                <div className="main__desctiption-text">
                    <div className="main__discription-text-name">
                        <h3 className="main__discription-text-name-1">Описание</h3>
                    </div>
                    <div className="main__description-paragraph">
                        <div>
                            Наша самая совершенная система двух камер.<br /> Особый взгляд на прочность
                            дисплея. <br /> Чип, с которым всё супербыстро.<br /> Аккумулятор держится заметно дольше.
                            <br /><i>iPhone
                                13
                                - сильный мира всего. </i>
                        </div>
                        <div >
                            Мы разработали совершенно новую схему расположения и развернулиобъективы на 45 градусов.
                            Благодаря этому
                            внутри
                            корпуса
                            поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной камеры.
                            Кроме
                            того, мы
                            освободили
                            место для системы оптической стабилизацииизображения сдвигом матрицы. И повысили скорость
                            работы
                            матрицы
                            насверхширокоугольной камере.
                        </div>
                        <div >
                            Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая
                            широкоугольная камера улавливает на 47% больше света для
                            более качественных фотографий и видео. Новая оптическая стабилизация сосдвигом матрицы
                            обеспечит
                            чёткие
                            кадры
                            даже в неустойчивом положении.
                        </div>
                        <div >
                            Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и
                            изменения
                            резкости. Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте
                            съёмки,
                            создавая
                            красивый
                            эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на
                            другогочеловека или
                            объект, который появился в кадре. Теперь ваши видео будут смотретьсякак настоящее кино.
                        </div>
                    </div>
                </div>
            </div>
            <div className="main__sidebar">
                <div className="main__sidebar-important">
                    <div className="main__sidebar-important-price">
                        <div className="main__sidebar-important-price-1">
                            <span className="main__sidebar-important-price-old "> 75 990</span>
                            <span onClick={addToFavorite} className="main__sidebar-important-favorite"><svg className="main__sidebar-important-favorite-icon" width="26" height="20"
                                viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                    fill={favoriteCount>0?'red' : '#888888'}
                                    d="M2.78502 2.57269C5.17872 0.274736 9.04661 0.274736 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.274736 22.8216 0.274736 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63043 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63043 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" />

                            </svg></span>
                        </div>
                        <span className="main__sidebar-important-actual-price "> 67 990₽ </span>
                    </div>
                    <div className="main__sidebar-delivery">
                        <span className="main__sidebar-option-delivery "> Самовывоз в четверг, 1 сентября — <b>бесплатно</b></span>
                        <span className="main__sidebar-option-delivery "> Курьером в четверг, 1 сентября — <b> бесплатно</b></span>
                    </div>
                    <button onClick={addToCart} id="add-cart" className={cartCount > 0 ? "main__sidebar-btn-important-add" : "main__sidebar-btn-important btn-guidance1"} > {cartCount > 0 ? 'Товар уже в корзине' : 'Добавить в корзину'} </button>
                </div>
                <div className="main__sidebar-advertisement">
                    <span className="main__sidebar-advertisement-name"> Реклама</span>
                    <div className="main__sidebar-advertisement-list">
                        <iframe title="market1" scrolling="no" className="main__sidebar-advertisement-list-frame1" src="frame/ads.html"></iframe>
                        <iframe title="market2" scrolling="no" className="main__sidebar-advertisement-list-frame1" src="frame/ads.html"></iframe>
                    </div>
                </div>
            </div>
        </div>

    )

}

export default MainCharacteristic;