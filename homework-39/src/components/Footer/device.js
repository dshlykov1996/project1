const size = {
mobileS: '360px',
mobileMax:'1023px'
}
export const device={
    mobileS:`(min-width:${size.mobileS})`,
    mobileMax:`(max-width:${size.mobileMax})`
}