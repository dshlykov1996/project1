import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './components/Header/Header';
import BreadCrumbs from './components/BreadCrumds/BreadCrumbs';
import MainName from './components/Main/MainName';
import MainPhotoProduct from './components/Main/MaimPhotoProduct';
import MainCharacteristic from './components/Main/MainCharacteristic';
import MainTable from './components/Main/MainTable';
import MainReviews from './components/Main/MainReviews';
import MainForm from './components/Main/MainForm';
import Footer from './components/Footer/Footer';
import HomePage from './components/Home/HomePage';

function App() {

  return (
    <>
    
      <BrowserRouter>
        <Header>  </Header>
        <Routes>
          <Route path='/' element={<HomePage />} />
          <Route path='/product' element={
            <>
              <BreadCrumbs item1='Электроника' next='>' item2='Смартфоны и гаджеты ' item3='Мобильные телефоны' item4='Apple' />
              <MainName name='Смартфон Apple iPhone 13, синий' />
              <MainPhotoProduct></MainPhotoProduct>
              <MainCharacteristic />
              <MainTable />
              <MainReviews />
              <MainForm />
            </>
          } />
        </Routes>
       <Footer />
      </BrowserRouter>
    </>
  );
}

export default App;
