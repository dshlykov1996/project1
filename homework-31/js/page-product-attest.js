'use strict';
const strorage = window.localStorage;
const button = document.querySelector(".main__form-feedback-btn");
const input1 = document.querySelector(".main__form-feedback-input1");
const input2 = document.querySelector(".main__form-feedback-input2");
const input1Error = document.querySelector(".main__form-input-error1");
const input2Error = document.querySelector(".main__form-input-error2");


const addBtn = document.getElementById('add-comment-btn');

const addToCart = document.getElementById('add-cart');
const cartCount = document.getElementById('header-count');

autoComplite();
cartInfoStorageInit();

input1.addEventListener('focus', handleFocus);
input2.addEventListener('focus', handleFocus);


input1.addEventListener('input', handleSave);
input2.addEventListener('input', handleSave);



addToCart.addEventListener('click', () => {
    const cartInfo = Number(strorage.getItem('cartInfo'));

    if (cartInfo === 0) {
        strorage.setItem('cartInfo', 1);
        addToCart.innerHTML = 'Товар уже в корзине';
        addToCart.style.backgroundColor = '#c4c4c4';
    }
    if (cartInfo > 0) {
        strorage.setItem('cartInfo', 0);
        addToCart.innerHTML = 'Добавить товар';
       
    }
 cartInfoStorageInit();
});

addBtn.addEventListener('click', () => {
    const name = nameInput.value.trim();
    const raiting = raitingInput.value.trim();
    

    const nameValid = validate('name', name);
    const raitingValid = validate('raiting', raiting);

    if (!nameValid.isValid) {
        input1Error.classList.remove('hide');
        input1Error.innerHTML = nameValid.content;
    }

    if (!raitingValid.isValid) {
        input2Error.classList.remove('hide');
        input2Error.innerHTML = raitingValid.content;
    }


    if (nameValid.isValid && raitingValid.isValid) {
        strorage.clear();
        input1.value = '';
        input2.value = '';
    }
});

function autoComplite() {
    const obj = Object.entries(strorage);

    obj.forEach((elem) => {
        const key = elem[0];
        const value = elem[1];

        if (key && key === 'name') {
            input1.value = value;
        }

        if (key && key === 'raiting') {
            input2.value = value;
        }
    });
}



function handleFocus() {

    const name = input1.value.trim();
    const raiting = input2.value.trim();

    const nameValid = validate('name', name).isValid;
    const raitingValid = validate('raiting', raiting).isValid;

   

    if (!nameValid) {
        storage.setItem('name', '');
        clearError(input1Error);
    }

    if (!raitingValid) {
        storage.setItem('rating', '');
        clearError(input2Error);
    }
    autoComplite();
}

function handleSave(event) {
    const value = event.target.value;

    const targetId = event.target.id;
    const nameId = 'add-comment-name';
    const raitingId = 'add-comment-raiting';
    

    if (targetId === nameId) {
        strorage.setItem('name', value);
    }

    if (targetId === raitingId) {
        strorage.setItem('raiting', value);
    }
}

function clearError(elem) {

    elem.innerHTML = '';
    elem.classList.add('hide');
}

function validate(type, value) {
    switch (type) {
        case 'name':
            if (value === '') {
                return { content: 'Вы забыли указать имя и фамилию', isValid: false };
            } else if (value.length <= 2) {
                return { content: 'Имя не может быть короче 2-хсимволов', isValid: false };
            } else {
                return { content: value, isValid: true };
            }
        case 'raiting':
            if (value === '') {
                return { content: 'поле не заполнено: Оценка должна быть от 1 до 5', isValid: false };
            } else if (isNaN(Number(value))) {
                return { content: 'в поле введены буквы: Оценка должна быть от 1 до 5', isValid: false };
            } else if (Number(value) < 1 || Number(value) > 5) {
                return {
                    content: 'ввели цифры больше 5 или меньше 1: Оценка должна быть от1 до 5',
                    isValid: false,
                };
            } else {
                return { content: value, isValid: true };
            }
        case 'text':
            if (value === '') {
                return { content: 'Введите хоть какой то техт...', isValid: false };
            } else return { content: value, isValid: true };
    }
}

function cartInfoStorageInit() {
    const cartInfo = strorage.getItem('cartInfo');

    if (cartInfo === null) {
        strorage.setItem('cartInfo', 0);
    }
    if (+cartInfo === 0) {

        addToCart.innerHTML = 'Добавить товар';
        cartCount.classList.add('hide');
    }
    if (+cartInfo > 0) {

        addToCart.innerHTML = 'Товар уже в корзине';
        cartCount.innerHTML = cartInfo;
        cartCount.classList.remove('hide');
        addToCart.style.backgroundColor = '#c4c4c4';
    }
}