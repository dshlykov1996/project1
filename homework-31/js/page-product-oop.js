class Form {
    constructor(errors) {
        this.errors = errors
    }

    handleFocus() {
        this._clearError(this.errors.input1Error);
        this._clearError(this.errors.input2Error);


    }
    clearError(elem) {
        elem.value = '';
        elem.classList.add('hide');
    }

}

class AddReviewForm extends Form {

    constructor(storage, errors, inputs) {
        super(errors)
        this.storage = storage;
        this.inputs = inputs;
    }

    autoComplite() {
        const obj = Object.entries(this.storage);
        obj.forEach((elem) => {
            const key = elem[0];
            const value = elem[1];

            if (key && key === 'name') {
                input1.value = value;
            }

            if (key && key === 'raiting') {
                input2.value = value;
            }
        });

    }

    addBtnHandler() {
        const name = this.input1.value.trim();
        const rating = this.input2.value.trim();

        const nameValid = this._validate('name', name);
        const ratingValid = this._validate('rating', rating);
        if (!nameValid.isValid) {
            input1Error.classList.remove('hide');
            input1Error.innerHTML = nameValid.content;
        }
        if (!ratingValid.isValid) {
            input2Error.classList.remove('hide');
            input2Error.innerHTML = ratingValid.content;
        }

        if (nameValid.isValid && ratingValid.isValid) {
            strorage.clear();
            input1.value = '';
            input2.value = '';
        }
    }
    handleSave(event) {
        const value = event.target.value;

        const targetClass = event.target.class;
        const nameClass = 'main__form-feedback-input1';
        const raitingClass = 'main__form-feedback-input2';
      

        if (targetClass === nameClass) {
            storage.setItem('name', value);
        }

        if (targetClass === raitingClass) {
            storage.setItem('raiting', value);
        }
    }

    _validate(value, type) {
        switch (type) {
            case 'name':
                if (value === '') {
                    return { content: 'Вы забыли указать имя и фамилию', isValid: false };
                } else if (value.length <= 2) {
                    return { content: 'Имя не может быть короче 2-хсимволов', isValid: false };
                } else {
                    return { content: value, isValid: true };
                }
            case 'raiting':
                if (value === '') {
                    return { content: 'Поле не заполнено: Оценка должна быть от 1 до 5', isValid: false };
                } else if (isNaN(Number(value))) {
                    return { content: 'В поле введены буквы: Оценка должна быть от 1 до 5', isValid: false };
                } else if (Number(value) < 1 || Number(value) > 5) {
                    return {
                        content: 'Ввели цифры больше 5 или меньше 1: Оценка должна быть от 1 до 5',
                        isValid: false,
                    };
                } else {
                    return { content: value, isValid: true };
                }
        }
    }
}

const strorage = window.localStorage;
const button = document.querySelector(".main__form-feedback-btn");
const input1 = document.querySelector(".main__form-feedback-input1");
const input2 = document.querySelector(".main__form-feedback-input2");
const input1Error = document.querySelector(".main__form-input-error1");
const input2Error = document.querySelector(".main__form-input-error2");

const inputObj = {
    input1,
    input2,
};

const errorObj = {
    input1Error,
    input2Error,
};


const addReviewForm = new AddReviewForm(inputObj, errorObj,strorage);
addReviewForm.autoComplite();

button.addEventListerner('click', addReviewForm.addBtnHandler.bind(addReviewForm));
input1.addEventListener('focus', addReviewForm.handleFocus.bind(addReviewForm));
input2.addEventListener('focus', addReviewForm.handleFocus.bind(addReviewForm));

input1.addEventListener('input', addReviewForm.handleSave.bind(addReviewForm));
input2.addEventListener('input', addReviewForm.handleSave.bind(addReviewForm));




