"use strict"
const button = document.querySelector(".main__form-feedback-btn");
const input1 = document.querySelector(".main__form-feedback-input1");
const input2 = document.querySelector(".main__form-feedback-input2");
const input1Error = document.querySelector(".main__form-input-error1");
const input2Error = document.querySelector(".main__form-input-error2");

input1.addEventListener('focus', handleFocus);
input2.addEventListener('focus', handleFocus);

button.addEventListener("click", () => {

    if (!input1.value.trim() && !input2.value) {
        input1Error.innerHTML = "Вы не указали имя и фамилию";
        input1Error.style.display = "block";
        input2Error.innerHTML = "Оценка должна быть от 1 до 5";
        input2Error.style.display = "block";
    }
    if (!input1.value.trim()) {
        input1Error.innerHTML = "Вы не указали имя и фамилию";
        input1Error.style.display = "block";
    }
    if (!input2.value) {
        input2Error.innerHTML = "Оценка должна быть от 1 до 5";
        input2Error.style.display = "block";
    }
    if (input1.value < 2) {
        input1Error.innerHTML = "Введено меньше двух символов";
        input1Error.style.display = "block";
    }

    if (input2.value < 1 || input2.value > 5) {
        input2Error.innerHTML = "Оценка должна быть от 1 до 5";
        input2Error.style.display = "block";
    }
});

function handleFocus(){
    clearError(input1Error);
    clearError(input2Error);
}
function clearError(elem) {
    elem.innerHTML = '';
    elem.classList.add('hide');
}
