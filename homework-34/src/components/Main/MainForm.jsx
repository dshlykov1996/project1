import React from "react";
import './mainFormFeedback.css';
import './mainFormFeedbackName.css';
import './mainFormFeedbackForm.css';
import './mainFormFeedbackInput.css';
import './mainFormFeedbackInput1.css';
import './mainFormInputError1.css';
import './mainFormFeedbackInput2.css';
import './mainFormInputError2.css';
import './mainFormFeedbackTextarea.css';
import './mainFormFeedbackBtn.css';

function MainForm(){

return(
    <div className="main__form-feedback">
    <span className="maim__form-feedback-name"> Добавить свой отзыв</span>
    <form className="main__form-feedback-form" name="myform" >
        <div className="main__form-feedback-input">
            <div className="main__form-input">
                <input  id="add-comment-name" className="main__form-feedback-input1 " type="text" name="name"
                    placeholder="Имя и фамилия" />
                <div className="main__form-input-error1"> </div>
            </div>

            <div className="main__form-input">
                <input value="" id="add-comment-raiting" className="main__form-feedback-input2" type="number" max="5" min="1" name="score"
                    placeholder="Оценка"  />
                <div className="main__form-input-error2"> </div>
            </div>
        </div>

        <div className="main__form-feedback-text">
            <textarea className="main__form-feedback-textarea" name="text" placeholder="Текст отзыва"></textarea>
        </div>
        <button  id="add-comment-btn" type="button" className="main__form-feedback-btn">Добавить отзыв </button>
    </form>

</div>

    
)
}
export default MainForm;