import Header from './components/Header/Header';
import BreadCrumbs from './components/BreadCrumds/BreadCrumbs';
import MainName from './components/Main/MainName';
import MainPhotoProduct from './components/Main/MaimPhotoProduct';
import MainCharacteristic from './components/Main/MainCharacteristic';
import MainTable from './components/Main/MainTable';
import MainReviews from './components/Main/MainReviews';
import MainForm from './components/Main/MainForm';
import Footer from './components/Footer/Footer';

function App() {

  return (
    <>
      <Header> </Header>
      <BreadCrumbs item1='Электроника' next='>' item2='Смартфоны и гаджеты ' item3='Мобильные телефоны' item4='Apple' />
      <MainName name='Смартфон Apple iPhone 13, синий' />
      <MainPhotoProduct></MainPhotoProduct> 
      <MainCharacteristic  />
      <MainTable/>
      <MainReviews/>
      <MainForm/>
      <Footer/>
    
      


    </>


  );
}

export default App;
