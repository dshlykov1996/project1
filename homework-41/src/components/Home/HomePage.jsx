import React from "react";
import './homePage.css';
import { Link } from "react-router-dom";
const HomePage = () => {
    return (
        <>
            <div className="home">
                <div className="homeContent">
                    Здесь должно быть содержимое главной страницы.
                    Но в рамках курса главная страница  используется лишь
                    в демонстрационных целях
                </div>
                <Link to={"/product"}> Перейти на страницу товара </Link>
                
            </div>



        </>

    )
}
export default HomePage;