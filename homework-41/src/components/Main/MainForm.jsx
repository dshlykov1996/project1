import React, { useEffect, useState } from "react";
import './mainFormFeedback.css';
import './mainFormFeedbackName.css';
import './mainFormFeedbackForm.css';
import './mainFormFeedbackInput.css';
import './mainFormFeedbackInput1.css';
import './mainFormInputError1.css';
import './mainFormFeedbackInput2.css';
import './mainFormInputError2.css';
import './mainFormFeedbackTextarea.css';
import './mainFormFeedbackText.css';
import './mainFormFeedbackBtn.css';

const storage = window.localStorage;
function MainForm() {

    const [name, setName] = useState(storage.getItem('name'));
    const [rating, setRating] = useState(storage.getItem('rating'));
    const [isPressAddCommentBtn, setisPressAddCommentBtn] = useState(false);

    const [nameValid, setNameValid] = useState({});
    const [raitingValid, setRaitingValid] = useState({});

    const changeName = (event) => {
        const value = event.target.value;
        storage.setItem('name', value);
        setName('value');
    };
    const changeRating = (event) => {
        const value = event.target.value;
        storage.setRating('rating', value);
        setRating('value');
    };

    const addComment = () => {
        setNameValid(validate('name', name));
        setRaitingValid('raiting', rating);
        setisPressAddCommentBtn(true);

    };

    const handleFocus = (type) => {
        switch (type) {
            case 'name':
                if (!nameValid.isValid && isPressAddCommentBtn) {
                    setName('');
                    storage.setItem('name', '');
                    setisPressAddCommentBtn(true);
                }
                break;
            case 'raiting':
                if (!raitingValid.isValid && isPressAddCommentBtn) {
                    setRating('');
                    storage.setItem('raiting', '');
                    setisPressAddCommentBtn(true);
                }
                break;
            default:
        }
    };

    useEffect(() => {
        if (isPressAddCommentBtn && nameValid.isValid && raitingValid.isValid) {
            storage.setItem('name', '');
            storage.setItem('raiting', '');
            setName('');
            setRating('');
        }
    }, [isPressAddCommentBtn]);

    function validate(type, value) {

        switch (type) {
            case 'name':
                if (value === '') {
                    return { content: 'Вы забыли указать имя и фамилию', isValid: false };
                } else if (value.length <= 2) {
                    return { content: 'Имя не может быть короче 2-х имволов', isValid: false };
                } else {
                    return { content: value, isValid: true };
                }
            case 'raiting':
                if (value === '') {
                    return { content: 'Поле не заполнено: Оценка должна быть от 1 до 5', isValid: false };
                } else if (isNaN(Number(value))) {
                    return { content: 'В поле введены буквы: Оценка должна быть от 1 до 5', isValid: false };
                } else if (Number(value) < 1 || Number(value) > 5) {
                    return {
                        content: 'Ввели цифры больше 5 или меньше 1: Оценка должна быть от 1 до 5',
                        isValid: false,
                    };
                } else {
                    return { content: value, isValid: true };
                }
            default:
        }
    }

    return (
        <div className="main__form-feedback">
            <span className="maim__form-feedback-name"> Добавить свой отзыв</span>
            <form className="main__form-feedback-form" name="myform" >
                <div className="main__form-feedback-input">
                    <div className="main__form-input">
                        <input id="add-comment-name" className="main__form-feedback-input1 " type="text" name="name" onInput={(event) => changeName(event)}
                            placeholder="Имя и фамилия" onFocus={() => handleFocus('name')} />
                        <div className={isPressAddCommentBtn && (nameValid && nameValid.isValid
                            ? 'main__form-input-error1 hide'
                            : 'main__form-input-error1')
                        }>  {isPressAddCommentBtn && (nameValid && nameValid.isValid ? '' : nameValid.content)}
                        </div>
                    </div>

                    <div className="main__form-input">
                        <input id="add-comment-raiting" className="main__form-feedback-input2" type="number" name="score" onInput={(event) => changeRating(event)}
                            placeholder="Оценка"  onFocus={() => handleFocus('raiting')} />
                        <div className={isPressAddCommentBtn && (raitingValid && raitingValid.isValid
                            ? 'main__form-input-error2 hide'
                            : 'main__form-input-error2')
                        } > {isPressAddCommentBtn && (raitingValid && raitingValid.isValid ? '' : raitingValid.content)} </div>
                    </div>
                </div>

                <div className="main__form-feedback-text">
                    <textarea className="main__form-feedback-textarea" name="text" placeholder="Текст отзыва"></textarea>
                </div>
                <button onClick={addComment} id="add-comment-btn" type="button" className="main__form-feedback-btn">Добавить отзыв </button>
            </form>

        </div>
    )
}
export default MainForm;