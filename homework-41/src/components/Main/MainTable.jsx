import React from "react";
import './mainTable.css';
import './mainTableName.css';
import './mainTableComparison.css';
import './mainTableComparisonLine.css';
import './mainTableComparisonLineText.css';
import './mainTableComparisionColumnText.css';
import './mainTableComparisionColumn.css';

function MainTable() {

    return (
        <div className="main__table">
            <span className="main__table-table-name">Сравнение моделей </span>
            <table className="main__table-comparison">
                <thead>
                    <tr className="main__table-comparison-line">
                        <th className="main__table-comparsion-line-text">Модель</th>
                        <th className="main__table-comparsion-line-text">Вес</th>
                        <th className="main__table-comparsion-line-text">Высота</th>
                        <th className="main__table-comparsion-line-text">Ширина</th>
                        <th className="main__table-comparsion-line-text">Толщина</th>
                        <th className="main__table-comparsion-line-text">Чип</th>
                        <th className="main__table-comparsion-line-text">Объём памяти</th>
                        <th className="main__table-comparsion-line-text">Аккумулятор</th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="main__table-comparison-column">
                        <td className="main__table-comparison-column-text"> iPhone 11</td>
                        <td className="main__table-comparison-column-text"> 194 грамма</td>
                        <td className="main__table-comparison-column-text"> 150.9 мм</td>
                        <td className="main__table-comparison-column-text"> 75.7 мм</td>
                        <td className="main__table-comparison-column-text"> 8.3 мм</td>
                        <td className="main__table-comparison-column-text"> A13 Bionic chip</td>
                        <td className="main__table-comparison-column-text"> до 128 ГБ</td>
                        <td className="main__table-comparison-column-text"> До 17 часов </td>
                    </tr>
                    <tr className="main__table-comparison-column">
                        <td className="main__table-comparison-column-text"> iPhone 12</td>
                        <td className="main__table-comparison-column-text"> 164 грамма</td>
                        <td className="main__table-comparison-column-text"> 146.7 мм</td>
                        <td className="main__table-comparison-column-text"> 71.5 мм</td>
                        <td className="main__table-comparison-column-text"> 7.4 мм</td>
                        <td className="main__table-comparison-column-text"> A14 Bionic chip</td>
                        <td className="main__table-comparison-column-text"> до 256 ГБ</td>
                        <td className="main__table-comparison-column-text"> До 19 часов </td>
                    </tr>
                    <tr className="main__table-comparison-column">
                        <td className="main__table-comparison-column-text"> iPhone 13</td>
                        <td className="main__table-comparison-column-text"> 174 грамма</td>
                        <td className="main__table-comparison-column-text"> 146.7 мм</td>
                        <td className="main__table-comparison-column-text"> 71.5 мм</td>
                        <td className="main__table-comparison-column-text"> 7.65 мм</td>
                        <td className="main__table-comparison-column-text"> A15 Bionic chip</td>
                        <td className="main__table-comparison-column-text"> до 512 ГБ</td>
                        <td className="main__table-comparison-column-text"> До 19 часов </td>
                    </tr>
                </tbody>

            </table>
        </div>
    )
}

export default MainTable;