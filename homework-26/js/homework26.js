"use strict"
//Упражнение 1
let arr = [3, "a", 5, 150, 10];
//console.log(getSumm(arr))
function getSumm(arr) {
    return arr.reduce(function (acc, curr) {
        if (typeof curr === 'number') {
            acc += curr
        } return acc;
    }, 0);
}
console.log("Сумма = ",getSumm(arr))


//Упражнение 3
let cart = [1242, 2547, 1458];

function addToCart(index) {
    if (cart.includes(index)) {
        console.log("Данный элемент уже добавлен", index);
    } else {
        cart.push(index);
    }
}

function delToCart(index) {
    if (cart.includes(index)) {
        cart.pop(index);
    } else {
        console.log("Нет такого элемента", index);
    }
}
addToCart(45);
console.log(cart);
delToCart(1458);
console.log(cart);

