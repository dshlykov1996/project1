import React from "react";
import './mainPhotoProduct.css';
import './mainPhotoProductList.css';
function MainPhotoProduct() {
    return (

        <div className="main__photo-product">
            <img className="main__photo-product-list" alt="рисунок 1" src='./main-photo-product/image-1.webp' />
            <img className="main__photo-product-list" alt="рисунок 2" src='./main-photo-product/image-2.webp' />
            <img className="main__photo-product-list " alt="рисунок 3" src='./main-photo-product/image-3.webp ' />
            <img className="main__photo-product-list" alt="рисунок 4" src='./main-photo-product/image-4.webp' />
            <img className="main__photo-product-list " alt="рисунок 5" src='./main-photo-product/image-5.webp' />
        </div>
    )

}

export default MainPhotoProduct;