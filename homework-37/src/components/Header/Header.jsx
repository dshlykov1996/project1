import React from 'react';
import { Link } from 'react-router-dom';
import header from './header.module.css';
import headerContent from './headerContent.module.css'
import headerBlock from './headerBlock.module.css';
import headerText from './headerText.module.css';
import headerTextColor from './headerTextColor.module.css';
import headerIconContent from './headerIconContent.module.css';
import headerIcon from './headerIcon.module.css';
import headerElectedCount from './headerElectedCount.module.css';
import headerBasketCount from './headerBasketCount.module.css';

function Header() {
    return (
        <header className={header.header}>
            <div className={headerContent.header__content} >
                <Link to = "/">
                <div className={headerBlock.header__block}>
                    <div>
                        <img src='./header-picturies/logo.png' />
                    </div>
                    <div >
                        <h1><a className={headerText.header__text} name="market"><span className={headerTextColor.header__text_color}>Цифровой</span> мир</a>
                        </h1>
                    </div>
                </div>
                </Link>

                <div className={headerIconContent.header__icon_content}>
                    <div className={headerIcon.header__icon}>
                        <div className={headerElectedCount.header__elected_count} >12</div>
                        <svg width="50" height="50" viewBox="0 0 51 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd"
                                d="M7.23828 10.9545C11.2278 7.12455 17.6743 7.12455 21.6638 10.9545L25.9301 15.0503L30.1966 10.9545C34.1862 7.12455 40.6326 7.12455 44.6222 10.9545C48.6116 14.7844 48.6116 20.973 44.6222 24.803L25.9301 42.7472L7.23828 24.803C3.24878 20.973 3.24878 14.7844 7.23828 10.9545ZM18.7175 13.7829C16.3552 11.5151 12.5469 11.5151 10.1846 13.7829C7.82224 16.0507 7.82224 19.7067 10.1846 21.9746L25.9301 37.0904L41.6759 21.9746C44.0382 19.7067 44.0382 16.0507 41.6759 13.7829C39.3137 11.5151 35.5053 11.5151 33.143 13.7829L25.9301 20.7072L18.7175 13.7829Z"
                                fill="#888888" />
                        </svg>
                    </div>

                    <div className={headerIcon.header__icon}>
                        <div id="header-count" className={headerBasketCount.header__basket_count}>0</div>
                        <svg width="42" height="41" viewBox="0 0 42 41" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd"
                                d="M0.720947 0.915039H8.01813H9.72605L10.061 2.5228L11.0138 7.09618H39.3531H41.8122L41.4081 9.42498L39.2618 21.7872L38.9716 23.4584H37.2068H14.4226L15.3062 27.7H37.2068V31.7H13.5983H11.8904L11.5554 30.0922L7.26297 9.4884L9.30586 9.09618L7.26297 9.4884L6.3102 4.91504H0.720947V0.915039ZM11.8471 11.0962L13.5893 19.4584H35.442L36.8937 11.0962H11.8471ZM19.2129 36.6778C19.2129 38.5186 17.6624 40.007 15.7449 40.007C13.8273 40.007 12.2769 38.5186 12.2769 36.6778C12.2769 34.837 13.8273 33.3876 15.7449 33.3876C17.6624 33.3876 19.2129 34.837 19.2129 36.6778ZM32.9162 40.007C34.8337 40.007 36.3841 38.5186 36.3841 36.6778C36.3841 34.837 34.8337 33.3876 32.9162 33.3876C30.9985 33.3876 29.4481 34.837 29.4481 36.6778C29.4481 38.5186 30.9985 40.007 32.9162 40.007Z"
                                fill="#888888" />
                        </svg>
                    </div>

                </div>
            </div>
        </header>

    );
}

export default Header;
