import React from "react";
import styled from "styled-components";
import { useCurrentDate } from "@kundinos/react-hooks";
import './footerContent.css';
import './footerContentLine.css';
import './footerContentColor.css';
import './footerReturn.css';
 
const Foot = styled.footer`
    display: flex;
margin: 100px 0 0 0;
align-items: center;
padding: 0px 50px;
background: #F2F2F2;
justify-content: space-between;
    `;
const FootContent = styled.div`
padding: 30px 0px;
font-size: 14px;
`;
const FooterContentLine =styled.div`
display:flex;
flex-direction:row;
font-weight: 700;
`;
const FooterContentColor = styled.div`
color: #f52;
`;
const FooterReturn = styled.div`
display: flex;
font-weight: 400;
font-size: 16px;
color:#008CF0;
`;

const Date = styled.p`
margin 0;
font-size: 14px;
`
function Footer() {
    const currentDate=useCurrentDate({every:"day"});

    const Year =  currentDate.getFullYear();
    return (
    <>
    <Foot>
        <FootContent>
            <FooterContentLine> © ООО « 
                <FooterContentColor>Цифровой&nbsp;</FooterContentColor> 
                 мир»,&nbsp;<Date>  2018 - {Year} </Date> <br />
                  </FooterContentLine>
                  Для уточнения информации звоните по номеру <a href="tel:7 900 000 0000">+7 900 000 0000</a>, <br />
                а предложения по сотрудничеству отправляйте на почту <a
                    href="mailto:partner@mdigital-mir.com">partner@digital-mir.com</a> <br />
        </FootContent>
        <FooterReturn> <a href="#market">Наверх</a></FooterReturn>
    </Foot>
    </>
    )
}
export default Footer;