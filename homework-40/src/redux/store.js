import { configureStore } from '@reduxjs/toolkit';
import cartSlice from './cartSlice';
import favoriteSlice from './favoriteSlice';

let actionCount =0;
 const logger  =(store)=> (next)=> (action)=>{
  const result = (actionCount+=1)  
  console.log ('Количество отработанных действмй: ', result );
  const res = next (action);
  store.getState();
  return res;

 }
export const store = configureStore({
  reducer: {
    cart: cartSlice,
    favorite: favoriteSlice,
  },  
  middleware:[logger],
});
