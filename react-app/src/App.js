import Header from './components/Header/Header';
import BreadCrumbs from './components/BreadCrumds/BreadCrumbs';

function App() {
  
  return (
    <> 
     <Header> </Header>
      <BreadCrumbs item1 = 'Электроника' next = '>' item2 = 'Смартфоны и гаджеты ' item3 = 'Мобильные телефоны' item4 = 'Apple' />  
    </>
      
  
  );
}

export default App;
