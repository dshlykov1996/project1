//Упражнение 1
const seconds = +prompt('Введите количество секунд');
timer(seconds);
function timer(time) {
    let number = time;
 
    const interval = setInterval(() => {
         number -= 1;
         console.log('Осталось = ', number);
        if (number <= 0) {
            clearInterval(interval);
            console.log('Время вышло');
        }
    }, 1000);
}

//Упражнение 2
const url= 'https://reqres.in/api/users';
const data = fetch (url);
const startDate=Date.now();
data
.then(function(request){
    return request.json();
})
.then(function(data){
    console.log('Количество пользователей = ', data.data.length);
    data.data.forEach(function(elem){
      console.log(`${elem.first_name} ${elem.last_name} (${elem.email})`);
      
    }); 
      console.log(Date.now()-startDate);
})
.catch(function(e){
    console.log(e)
})
